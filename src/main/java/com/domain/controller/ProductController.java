package com.domain.controller;

import com.domain.models.entities.Product;
import com.domain.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    // fungsi untuk membuat data baru di dalam database
    @PostMapping
    public Product createOne(@RequestBody Product product){
        return productService.save(product);
    }

    // fungsi untuk mengambil semua data di dalam database
    @GetMapping
    public Iterable<Product> findAll(){
        return productService.findAll();
    }

    // fungsi untuk mengambil data berdasarkan id
    @GetMapping({"/{id}"})
    public Product findOne(@PathVariable("id") Long id){
        return productService.findOne(id);
    }

    // fungsi untuk meng updata data dengan parameter id
    @PutMapping
    public Product update(@RequestBody Product product){
        return  productService.save(product);
    }

    // fungsi untuk men delete data dengan parameter id
    @DeleteMapping({"/{id}"})
    public void removeOne(@PathVariable("id") Long id){
        productService.removeOne(id);
    }

}
